package org.opencv.samples.facedetect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.objdetect.CascadeClassifier;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

public class FdActivity extends Activity implements CvCameraViewListener2 {

    private static final String    TAG                 = "OCVSample::Activity";
    private static final Scalar    FACE_RECT_COLOR     = new Scalar(0, 255, 0, 255);
    public static final int        JAVA_DETECTOR       = 0;
    public static final int        NATIVE_DETECTOR     = 1;
    public static final int		   BACK			   		= 0;
    public static final int 	   FRONT 			   = 1;
    
    public static final int 	   MUS1 			  = 0;
    public static final int 	   MUS2 			  = 1;
    public static final int 	   GLASS1			  = 2;
    public static final int		   SUNGLASS			  = 3;
    public static final int	       FUMANCHU			  = 4;
    public static final int        BEARD	          = 5;

    private MenuItem               mItemType;
    private MenuItem		       mItemCamera;
    private MenuItem			   mItemMus1;
    private MenuItem			   mItemMus2;
    private MenuItem               mItemGlass1;
    private MenuItem 			   mItemFuManchu;
    private MenuItem			   mItemSunglass;
    private MenuItem			   mItemBeard;
    private MenuItem			   mItemRotate;

    private Mat                    mRgba;
    private Mat                    mGray;
    private File                   mCascadeFile;
    private CascadeClassifier      mJavaDetector;
    private DetectionBasedTracker  mNativeDetector;

    private int                    mDetectorType       = JAVA_DETECTOR;
    private String[]               mDetectorName;
    private String[]			   mCameraName;
    private int 				   mCameraType = BACK;

    private float                  mRelativeFaceSize   = 0.2f;
    private int                    mAbsoluteFaceSize   = 0;

    private CameraBridgeViewBase   mOpenCvCameraView;
    
    private CascadeClassifier      mEyeJavaDetector;
    private DetectionBasedTracker  mEyeNativeDetector;
    
    public static List<String> accPaths; 
	public static int [] resourceIds = {R.raw.mustache, R.raw.mustache2, R.raw.glasses, R.raw.sunglasses, R.raw.fumanchu, R.raw.beard};
    public int curAcc = 0;
    
    // For presentation
    private boolean drawBox = false;
    private boolean loadPhotos = false;
    public static List<String> peoplePaths;
    public static int [] testIds = {R.raw.francois_linkedin, R.raw.silvio1, R.raw.silvio2};
    public static final int 			FRANCOIS1 = 0;
    public static final int				SILVIO1  = 1;
    public static final int				SILVIO2 = 2;
    public int curTestPic = SILVIO1;
    public boolean				   useEyes = false;

    /*
     * Loading and Setup
     */
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
    private boolean USELBP = true;
		@Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("detection_based_tracker");

                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        if (USELBP){
                            mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
                        } else {
                            is = getResources().openRawResource(R.raw.haarcascade_frontalface_alt2);
                            mCascadeFile = new File(cascadeDir, "haarcascade_frontalface_alt2.xml");
                        }
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mJavaDetector.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetector = null;
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

                        mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }
                    
                    // Load Eye XML / detector
                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.haarcascade_eye);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "haarcascade_eye.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mEyeJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mEyeJavaDetector.empty()) {
                            Log.e(TAG, "Failed to load eye cascade classifier");
                            mEyeJavaDetector = null;
                        } else
                            Log.i(TAG, "Loaded eye cascade classifier from " + mCascadeFile.getAbsolutePath());

                        mEyeNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load eye cascade. Exception thrown: " + e);
                    }
                    
                    // load mustache picture paths from application resources
                	File picDir = getPictureStorageDir();
                	accPaths = new ArrayList<String>();
                	for (int i = 0; i < resourceIds.length; ++i)
                	{
                		try 
                		{
                			File imgFile = new File(picDir, "finalProj-"+i+".jpg");
                			InputStream is = getResources().openRawResource(resourceIds[i]);
                			OutputStream os = new FileOutputStream(imgFile);
                			byte[] data = new byte[is.available()];
                			is.read(data);
                			os.write(data);
                			is.close();
                			os.close();
                			accPaths.add(imgFile.getPath());
                			Log.i("info", "Initialize: " + imgFile.getPath());
                		}
                        catch(IOException e)
                        {
                        	Log.e("Issue: ", e.getMessage());
                        	Log.e("IO", "Failed to write test image Here!");
                        }                    	
                	}
                	
                	if (loadPhotos) {
                		// Load pictures from storage
                    	picDir = getPictureStorageDir();
                    	peoplePaths = new ArrayList<String>();
                    	for (int i = 0; i < testIds.length; ++i)
                    	{
                    		try 
                    		{
                    			File imgFile = new File(picDir, "finalProjPeople-"+i+".jpg");
                    			InputStream is = getResources().openRawResource(testIds[i]);
                    			OutputStream os = new FileOutputStream(imgFile);
                    			byte[] data = new byte[is.available()];
                    			is.read(data);
                    			os.write(data);
                    			is.close();
                    			os.close();
                    			peoplePaths.add(imgFile.getPath());
                    			Log.i("info", "Initialize: " + imgFile.getPath());
                    		}
                            catch(IOException e)
                            {
                            	Log.e("Issue: ", e.getMessage());
                            	Log.e("IO", "Failed to write test image Here!");
                            }                    	
                    	}
                	}



                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

	static private File getPictureStorageDir() {
		
		File picDir = new File(Environment.getExternalStorageDirectory(), "FinalProj-Pictures");
		if (!picDir.exists())
		{
			try
			{
				picDir.mkdir();
				Log.i("IO", "Final Project Pic Dirs: " + picDir);
			} catch (Exception e)
			{
				Log.e("IO", "Encountered exception while creating picture dir.");
				e.printStackTrace();
			}
		}
		return picDir;
	}
	
    public FdActivity() {
        mDetectorName = new String[2];
        mDetectorName[JAVA_DETECTOR] = "Java";
        mDetectorName[NATIVE_DETECTOR] = "Native (tracking)";
        mCameraName = new String[2];
        mCameraName[BACK] = "Back Camera";
        mCameraName[FRONT] = "Front Camera";
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.face_detect_surface_view);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    /*
     * Process image frame
     */
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

    	long startTime = System.nanoTime();
    	
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

    	
    	// For demo, use preloaded pictures
    	if (loadPhotos) {
    		mRgba = loadFaces(mRgba.size());
    		Imgproc.cvtColor(mRgba, mGray, Imgproc.COLOR_RGBA2GRAY);
    	}
        
        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
            mNativeDetector.setMinFaceSize(mAbsoluteFaceSize);
        }

        MatOfRect faces = new MatOfRect();
        
        if (mDetectorType == JAVA_DETECTOR) {
            if (mJavaDetector != null) {
                mJavaDetector.detectMultiScale(mGray, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
                        new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
            }
        }
        else if (mDetectorType == NATIVE_DETECTOR) {
            if (mNativeDetector != null)
                mNativeDetector.detect(mGray, faces);
        }
        else {
            Log.e(TAG, "Detection method is not selected!");
        }

        Rect[] facesArray = faces.toArray();
        
        // Find Largest
        int largestSoFar = 0;
        int largestIndex = 0;
        for (int i = 0; i < facesArray.length; i ++) {
        	if (largestSoFar < (int)facesArray[i].area()) {
        		largestSoFar = (int)facesArray[i].area();
        		largestIndex = i;
        	}
        }
        
        // Calculate Eye positions
        int eye1Index = -1;
        int eye2Index = -1;
        Rect[] eyesArray; 
        Point midPoint1 = new Point();
        Point midPoint2 = new Point();
        
        if (useEyes && facesArray.length > 0) {
        	Rect faceRect = facesArray[largestIndex];
        	Mat grayFace = mGray.submat(faceRect);
	        MatOfRect eyes = new MatOfRect();
	        
	        if (mDetectorType == JAVA_DETECTOR) {
	            if (mEyeJavaDetector != null) {
	            	mEyeJavaDetector.detectMultiScale(grayFace, eyes, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
	                        new Size(), new Size(mAbsoluteFaceSize, mAbsoluteFaceSize));
	            }
	        }
	        else if (mDetectorType == NATIVE_DETECTOR) {
	            if (mEyeNativeDetector != null)
	            	mEyeNativeDetector.detect(grayFace, eyes);
	        }
	        else {
	            Log.e(TAG, "Eye Detection method is not selected!");
	        }
	
	        eyesArray = eyes.toArray();
	        
	        // Find eye pair
	        // This could use tweaking
	        for (int i = 0; i < eyesArray.length; i++){
	        	for (int j = i+1; j < eyesArray.length; j++){
	        		Rect eyeRect1 = eyesArray[i];
	        		Rect eyeRect2 = eyesArray[j];
	        		
	        		if (eyeRect1.x+eyeRect1.width < eyeRect2.x || eyeRect2.x + eyeRect2.width < eyeRect1.x){
	        			if (eyeRect1.y < (2*eyeRect2.y + eyeRect2.height)/2 && eyeRect2.y < (2*eyeRect1.y + eyeRect1.height)/2){
	        				eye1Index = i;
	        				eye2Index = j;
	        				break;
	        			}
	        		}
	        	}
	        }
	        
	        if (eye1Index >= 0) {
		        // Translate to original face coordinates
		        eyesArray[eye1Index].x = eyesArray[eye1Index].x + faceRect.x;
		        eyesArray[eye1Index].y = eyesArray[eye1Index].y + faceRect.y;
		        eyesArray[eye2Index].x = eyesArray[eye2Index].x + faceRect.x;
		        eyesArray[eye2Index].y = eyesArray[eye2Index].y + faceRect.y;
		        
		        
		        
		        midPoint1 = new Point((2* eyesArray[eye1Index].x + eyesArray[eye1Index].width)/2,(2*eyesArray[eye1Index].y + eyesArray[eye1Index].height)/2);
		        midPoint2 = new Point((2* eyesArray[eye2Index].x + eyesArray[eye2Index].width)/2,(2*eyesArray[eye2Index].y + eyesArray[eye2Index].height)/2);
	
		        if (drawBox) {
		        	// Draw eyes
			        Core.rectangle(mRgba, eyesArray[eye1Index].tl(), eyesArray[eye1Index].br(), FACE_RECT_COLOR, 1);
			        Core.rectangle(mRgba, eyesArray[eye2Index].tl(), eyesArray[eye2Index].br(), FACE_RECT_COLOR, 1);
		        	// Draw line between
		        	Core.line(mRgba, midPoint1, midPoint2, FACE_RECT_COLOR);
		        }
	        }
        }
        
        /*
         * Draw Accessory
         */
        //Draws a rectangle for only one box
        // Also means at least one rect exists
        if (largestIndex < facesArray.length) {
        	if (drawBox) {
        		Core.rectangle(mRgba, facesArray[0].tl(), facesArray[0].br(), FACE_RECT_COLOR, 3);
        	}
        	
        	// Combine images is where most of the magic is
            Mat acc = loadAcc();
            
            if (eye1Index >= 0) {
            	// Calculate angle
            	Point translatedPt = new Point(midPoint1.x - midPoint2.x, midPoint1.y - midPoint2.y);
            	float angle = (float) Math.atan2(translatedPt.y, -translatedPt.x);          	
            	if (angle < -Math.PI / 2) {
            		angle = (float) (Math.PI + angle);
            	}
            	int len = Math.max(acc.rows(), acc.cols());
            	Point pt = new Point(len/2, len/2);
            	Mat rot = Imgproc.getRotationMatrix2D(pt, angle * 180 / Math.PI, 1);
            	Imgproc.warpAffine(acc, acc, rot, acc.size(), Imgproc.INTER_LINEAR, Imgproc.BORDER_CONSTANT, new Scalar(255));
            }
            
            Mat combined = combineImages(mRgba, acc, facesArray[0]);
            
            long endTime = System.nanoTime();
            Log.i("System Timer", "total time: " + (endTime - startTime));
            mRgba = combined;
        }
        
        return mRgba;
    }

    /*
     * Combine input and accesory
     */
    private Mat combineImages(Mat orig, Mat extra, Rect interestRect) {
		// Make new image size of orig
    	Mat original = orig.clone();
    	Mat acc = extra.clone();
    	//Scalar zero = new Scalar(0);
    	//Mat templateImage = new Mat(orig.size(), orig.type(), zero);
    	
    	// calculate roi from face
    	Rect roiRect;
    	if (curAcc == MUS1 || curAcc == MUS2){
    	    if (curTestPic == SILVIO1) {
        		roiRect = new Rect(interestRect.x + interestRect.width * 7/24, interestRect.y + interestRect.height * 5/8, interestRect.width/2, interestRect.height/8);
    	    } else {
        		roiRect = new Rect(interestRect.x + interestRect.width/4, interestRect.y + interestRect.height * 5/8, interestRect.width/2, interestRect.height/8);
    	    }
    	} else if (curAcc == GLASS1 || curAcc == SUNGLASS){
        	//roiRect = new Rect(interestRect.x, interestRect.y, interestRect.width, interestRect.height);
        	roiRect = new Rect(interestRect.x + interestRect.width/16, interestRect.y + interestRect.height / 8, interestRect.width * 7 / 8, interestRect.height/2);
    		
    		// Francois test image
    		//roiRect = new Rect(interestRect.x + interestRect.width/32, interestRect.y + interestRect.height / 8, interestRect.width * 7 / 8, interestRect.height/2);
    	} else if (curAcc == FUMANCHU){
    		if (curTestPic == FRANCOIS1) {
    			roiRect = new Rect(interestRect.x + interestRect.width / 5, interestRect.y + interestRect.height * 19 / 48, interestRect.width * 9/16, interestRect.height*2/3);
    		} else {
    			roiRect = new Rect(interestRect.x + interestRect.width/4, interestRect.y + interestRect.height /2, interestRect.width/2, interestRect.height*2/3);
    		}
    	} else if (curAcc == BEARD) {
        	roiRect = new Rect(interestRect.x, interestRect.y + interestRect.height * 3/16, interestRect.width, interestRect.height);
    	} else {
        	roiRect = new Rect(interestRect.x, interestRect.y, interestRect.width, interestRect.height);
    	}
    	// resize extra to roi
    	Imgproc.resize(acc, acc, roiRect.size());
    	
    	// copy extra into new image
    	Mat gray = new Mat();
    	Mat mask = new Mat();
    	Imgproc.cvtColor(acc, gray, Imgproc.COLOR_RGBA2GRAY);
    	int thresh = 64;
    	
    	if (curAcc == MUS1) {
    		thresh = 64;
        	Imgproc.threshold(gray, mask, thresh, 1, Imgproc.THRESH_BINARY_INV);
    	} else if (curAcc == MUS2) {
    		thresh = 50;
        	Imgproc.threshold(gray, mask, thresh, 1, Imgproc.THRESH_BINARY);
    	} else if (curAcc == GLASS1) {
    		thresh = 32;
        	Imgproc.threshold(gray, mask, thresh, 1, Imgproc.THRESH_BINARY_INV);
    	} else if (curAcc == SUNGLASS) {
    		thresh = 64;
        	Imgproc.threshold(gray, mask, thresh, 1, Imgproc.THRESH_BINARY_INV);
    	} else if (curAcc == FUMANCHU) {
    		thresh = 64;
        	Imgproc.threshold(gray, mask, thresh, 1, Imgproc.THRESH_BINARY_INV);
    	} else if (curAcc == BEARD) {
    		thresh = 64;
        	Imgproc.threshold(gray, mask, thresh, 1, Imgproc.THRESH_BINARY_INV);
    	}
    	
    	
    	Mat roi = original.submat(roiRect);
    	acc.copyTo(roi, mask);
    	Log.i("Mask", "MAsk size: " + mask.depth() + " " + mask.channels());
    	
    	// Create weightmap
    	
    	// Combine images with weight map
    	
    	Mat finalMat = original;
    	return finalMat;
	}
    
    /*
     * Load test images
     * 		For presentation / testing
     */
	private Mat loadFaces(Size size) {
		try {
			Mat image = Highgui.imread(peoplePaths.get(curTestPic));
			image.convertTo(image, CvType.CV_8UC3);
			Imgproc.cvtColor(image,image, Imgproc.COLOR_RGB2BGRA);
			
			if (curTestPic != SILVIO1) {
				Imgproc.resize(image, image, size);
			} else {
				Imgproc.resize(image, image, new Size(), 2.0, 2.0, Imgproc.INTER_LINEAR );
				Mat blank = new Mat(size, image.type(), new Scalar(0));
				Rect rectangle = new Rect (0,0, image.cols(), image.rows());
				Mat subMat = blank.submat(rectangle);
				image.copyTo(subMat);
				image = blank;
			}
			
	    	return image;
		} catch (IOError e) {
			Log.i("TAG", e.toString());
			return null;
		}

	}

	/*
	 * Load Accesory image
	 */
	private Mat loadAcc() {
    	// Try mustache stuff
        Mat image = Highgui.imread(accPaths.get(curAcc));
        image.convertTo(image, CvType.CV_8UC3);
        Imgproc.cvtColor(image, image, Imgproc.COLOR_RGB2BGRA);
        if (image.empty())
        {
        	Log.i("Image Read", "Image not read");
        } else {
        	Log.i("HERE", "Image read now");
           
        }
        
        return image;
	}

	/*
	 * Menu Items
	 */
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
        mItemMus1 = menu.add("Black Mustache");
        mItemMus2 = menu.add("White Mustache");
        mItemGlass1 = menu.add("Glasses");
        mItemSunglass = menu.add("Sun Glasses");
        mItemFuManchu = menu.add("Fu Manchu");
        mItemBeard = menu.add("Beard");
        mItemType   = menu.add(mDetectorName[mDetectorType]);
        mItemCamera = menu.add(mCameraName[mCameraType]);
        mItemRotate = menu.add("Toggle Rotate");
        return true;
    }

	/*
	 * Different button presses
	 */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        
        if (item == mItemMus1)
        	setAcc(MUS1);
        else if (item == mItemMus2)
        	setAcc(MUS2);
        else if (item == mItemGlass1)
        	setAcc(GLASS1);
        else if (item == mItemSunglass)
        	setAcc(SUNGLASS);
        else if (item == mItemFuManchu)
        	setAcc(FUMANCHU);
        else if (item == mItemBeard){
        	setAcc(BEARD);
        }
        else if (item == mItemType) {
            mDetectorType = (mDetectorType + 1) % mDetectorName.length;
            item.setTitle(mDetectorName[mDetectorType]);
            setDetectorType(mDetectorType);
        }
        else if (item == mItemCamera) {
        	mCameraType = (mCameraType + 1) % mCameraName.length;
        	item.setTitle(mCameraName[mCameraType]);
        	setCameraType(mCameraType);
        	
        	// Menu camera is front, so switch to back
        	if (mCameraType == FRONT) {
        		startActivity(new Intent(FdActivity.this, FdActivityBack.class));
        	}
        }
        else if (item == mItemRotate){
        	if (useEyes == false){
        		useEyes = true;
        		Log.i("Rotate", "Rotation turned on");
        	} else {
        		useEyes = false;
        		Log.i("Rotate", "Rotation turned off");
        	}
        }
        	
        return true;
    }

    private void setAcc(int acc) {
		curAcc = acc;
	}

	private void setMinFaceSize(float faceSize) {
        mRelativeFaceSize = faceSize;
        mAbsoluteFaceSize = 0;
    }

    private void setDetectorType(int type) {
        if (mDetectorType != type) {
            mDetectorType = type;

            if (type == NATIVE_DETECTOR) {
                Log.i(TAG, "Detection Based Tracker enabled");
                mNativeDetector.start();
            } else {
                Log.i(TAG, "Cascade detector enabled");
                mNativeDetector.stop();
            }
        }
    }
    
    private void setCameraType(int type) {
        if (mCameraType != type) {
        	mCameraType = type;

            if (type == FRONT) {
                Log.i(TAG, "Front Camera On");
            } else {
                Log.i(TAG, "Back Camera On");
            }
        }
    }
}
